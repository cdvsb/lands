﻿

namespace Lands.Models
{
    public class WSResponse
    {
        public string Estatus { get; set; }
        public string CodigoErr { get; set; }
        public string Mensaje { get; set; }
    }
}
