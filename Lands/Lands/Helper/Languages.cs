﻿
namespace Lands.Helper
{
    using Xamarin.Forms;
    using Interfaces;
    using Resources;

    public static class Languages
    {
        static Languages()
        {
            var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            Resource.Culture = ci;
            DependencyService.Get<ILocalize>().SetLocale(ci);
        }
        public static string Error
        {
            get { return Resource.Error; }
        }
        public static string EmailValidation
        {
            get { return Resource.EmailValidation; }
        }
        public static string Accept
        {
            get { return Resource.Accept; }
        }
        public static string PasswordValidation
        {
            get { return Resource.PasswordValidation; }
        }
        public static string UnknownError
        {
            get { return Resource.UnknownError; }
        }
        public static string EmailPlaceHolder
        {
            get { return Resource.EmailPlaceHolder; }
        }
        public static string PassPlaceHolder
        {
            get { return Resource.PassPlaceHolder; }
        }
        public static string Rememberme
        {
            get { return Resource.Rememberme; }
        }
        public static string PswLabel
        {
            get { return Resource.PswLabel; }
        }
        public static string ForgotPsw
        {
            get { return Resource.ForgotPsw; }
        }
        public static string RegisterLabel
        {
            get { return Resource.RegisterLabel; }
        }
    }

}
